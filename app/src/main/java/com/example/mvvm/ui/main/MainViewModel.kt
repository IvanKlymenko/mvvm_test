package com.example.mvvm.ui.main

import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel()

sealed class PointUiState {
//    data class Success(var point: PointsAdapter.Item?): PointUiState()
//    data class Error(var exception: Throwable): PointUiState()
}

sealed class PointUIEvent {
    data class ViewCreated(val pointId: String) : PointUIEvent()
}