package com.example.mvvm.repository.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.mvvm.requests.repos.RepoResponseItem

/**
 * @author ivan.a.klymenko@gmail.com on 3/19/21
 */
@TypeConverters(TypeConverter::class)
@Database(entities = [RepoResponseItem::class], version = 1, exportSchema = false)
abstract class XingDatabase : RoomDatabase() {
    abstract fun dbDao(): DbDao
}
