package com.example.mvvm.repository.api

import com.example.mvvm.requests.repos.RepoResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * @author ivan.a.klymenko@gmail.com on 3/19/21
 */

interface RemoteDataService {

    companion object {
        const val BASE_URL = "https://api.github.com/"
        const val API_KEY = "fccb554faa287ef5f2058174229abcb778b7a3ec"
    }

    @GET("orgs/{owner}/repos")
    fun getRemoteData(@Path("owner") owner: String): Flow<RepoResponse>


}
