package com.example.mvvm.repository.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.mvvm.requests.repos.RepoResponseItem
import kotlinx.coroutines.flow.Flow

/**
 * @author ivan.a.klymenko@gmail.com on 3/19/21
 */
@Dao
interface DbDao {

    @Query("SELECT * FROM repo_items")
    fun flowRepos(): Flow<List<RepoResponseItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(movies: List<RepoResponseItem>)

}
