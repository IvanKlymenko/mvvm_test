package com.example.mvvm.requests.repos


data class Permissions(
    val admin: Boolean,
    val push: Boolean,
    val pull: Boolean
)